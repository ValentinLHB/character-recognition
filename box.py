import pytesseract as tess
tess.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
from PIL import Image
import re

# EN: This is my try at creating a program that can process a jpeg image 
# to extract text and numbers. As an overwatch player, I wanted to keep track
# of the games I played: wins and losses, heroes played, maps ... But I wanted
# to automatize this process as it can be very time-consuming

# FR: Ceci est mon essai sur la création d'un programme capable d'extraire 
# du texte et les chiffres d'une image en jpeg. Ce programme trouve une 
# application pratique en tant que joueur d'Overwatch: il permet de tirer 
# de l'information des parties jouées: victoire ou défaite, sur quelle 
# carte, avec quel héros ... Et d'automatiser ce processusqui peut
# prendre du temps


support_heroes = [
    "ANA", "BAPTISTE", "BRIGITTE", "LÚCIO", 
    "MERCY", "MOIRA", "ZENYATTA"
]
tank_heroes = [
    "DVA", "ORISA", "REINHARDT", "ROADHOG", 
    "SIGMA", "WINSTON", "WRECKING BALL", "ZARYA"
]
damage_heroes = [
    "ASHE", "BASTION", "DOOMFIST", "ECHO", "GENJI",
    "HANZO", "JUNKRAT", "MCCREE", "MEI", "PHARAH",
    "REAPER", "SOLDIER", "SOMBRA", "SYMMETRA", "TORBJÖRN",
    "TRACER", "WIDOWMAKER"
]

escort_maps = [
    "DORADO", "HAVANA", "JUNKERTOWN", "RIALTO",
    "ROUTE", "WATCHPOINTGIBRALTAR"
]
control_maps = []
hybrid_maps = []
assault_maps = []

class Area:
    def __init__(self):
        pass

    def process(self, path, left, top, width, height, *hero):
        box = (left, top, left+width, top+height)
        img = Image.open(path)
        area = img.crop(box)
        area.show()
        text = tess.image_to_string(area)

        if "VICT" or "DEF" or "DRAW" in text:
            
            if "VICT" in text:
                text = "Win"
            elif "DEF" in text:
                text = "Loss"
            elif "DRAW" in text:
                text = "Draw"

        if "TIME" in text:
            check_match_time = re.compile(r'\d{2}:\d{2}')
            text = check_match_time.search(text).group()

        if hero: # Remove special characters with str.isalnum()
            alphanumeric = ""
            for character in text:
                if character.isalnum():
                    alphanumeric += character
            
            text = alphanumeric

        else:
            pass

        return text

coordinates = [ # Tell python where to look on the image
    {
        'result' : {
            'left' : 50,
            'top' : 50,
            'width' : 200,
            'height' : 50
        }
    },
    {
        'match_time' : {
            'left' : 220,
            'top' : 68,
            'width' : 350,
            'height' : 25
        }
    },
    {
        'hero_played' : {
            'left' : 100,
            'top' : 190,
            'width' : 350,
            'height' : 90
        }
    },
    {
        'map_played' : {
            'left' : 220,
            'top' : 42,
            'width' : 350,
            'height' : 25
        }
    }
]

img = "screenshots/screenshot.jpg"

# Get result (win or lose)
result = Area().process(
    img, 
    coordinates[0]['result']['left'], 
    coordinates[0]['result']['top'], 
    coordinates[0]['result']['width'], 
    coordinates[0]['result']['height']
)
# print(result)

# Get match time
match_time = Area().process(
    img,
    coordinates[1]['match_time']['left'],
    coordinates[1]['match_time']['top'],
    coordinates[1]['match_time']['width'],
    coordinates[1]['match_time']['height']
)
# print(match_time)

# Get hero
# hero_played = Area().process(
#     img,
#     coordinates[2]['hero_played']['left'],
#     coordinates[2]['hero_played']['top'],
#     coordinates[2]['hero_played']['width'],
#     coordinates[2]['hero_played']['height'],
#     True
# )
# print(hero_played)

# Get map
map_played = Area().process(
    img,
    coordinates[3]['map_played']['left'],
    coordinates[3]['map_played']['top'],
    coordinates[3]['map_played']['width'],
    coordinates[3]['map_played']['height']
)

map_played = re.sub('[^A-Z]', '', map_played)
# print(map_played)

# Get role based on hero
# def find_role(hero):
#     if hero in support_heroes:
#         role = "Support"
#     elif hero in tank_heroes:
#         role = "Tank"
#     elif hero in damage_heroes:
#         role = "Damage"
#     else:
#         pass

#     return role

# role_played = find_role(hero=hero_played)
# print(role_played)

# def find_mode(mapp):
#     if mapp in control_maps:
#         mode = "Control"
#     if mapp in assault_maps:
#         mode = "Assault"
#     if mapp in escort_maps:
#         mode = "Escort"
#     if mapp in hybrid_maps:
#         mode = "Hybrid"
#     return mode

# mode_played = find_mode(mapp=map_played)
# print(mode_played)

# Store all game info in list
game_info = [
    result,
    match_time,
    # hero_played,
    # role_played,
    map_played,
    # mode_played
]
print(game_info)


# Improving the code requires skills and knowledge on Python and Image processing
# that I don't have. Rading the game result, match time, map played and hero_played 
# (when displayed) was the best I could do, with some errors here and there: 
# see yourself and try other screenshots, and feel free to improve this code